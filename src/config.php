<?php

return [
    // json 检测格式
    'jsonlint' => [
        '_class' => \Seld\JsonLint\JsonParser::class,
    ],
    // find 命令
    'finder' => [
        '_class' => \Symfony\Component\Finder\Finder::class,
    ],
    // http 请求
    'http' => [
        '_class' => \GuzzleHttp\Client::class,
    ],
    // filesystem 文件管理系统
    'filesystem' => [
        '_class' => \Symfony\Component\Filesystem\Filesystem::class
    ],
    // uuid
    'uuid' => [
        '_class' => function(){
            return \Ramsey\Uuid\Uuid::getFactory();
        }
    ],
    'faker' => [
        '_class' => function(){
            return \Faker\Factory::create();
        }
    ]
];