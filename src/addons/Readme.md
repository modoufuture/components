1. config
```php
return [
    // 沙箱路径
    'sandbox_path' => '',
    // 临时目录
    'tmp_path' => '',
    // 保存目录
    'save_path' => '',
    // 仓库地址
    'repositories' => [
        'base' => 'http://localhost:8081',
        'extra' => 'http://localhost:8082',
        'other' => '',
    ],
    // 仓库验证信息
    'auth' => [
        'base' => false, // 不需要验证
        'extra' => ['username', 'password'], // 基于http的用户名密码验证
        'other' => 'get_other_user_token', // 自定义回调,   function get_other_user_token($options){/*do more*/return $options;}
    ],
];
```

2. package info
```php
return [
    'name' => '包标识',
    'title' => '包标题',
    'intro' => '包简介',
    'type' => '包类型', // 自定义， 如: module,  theme, ... 
    'source' => [
        'type' => 'zip',
        'url' => 'http://localhost:8083/packages/packages.zip',
    ],
    'author' => [
        [
            'name' => '',
            'email' => '',
        ],
    ],
    'require' => [
        'module' => [
            'log' => '>=1.0',
            'database' => '<1.0',
        ],
        'javascript' => [
            'jquery' => '>=1.12'
        ]
    ],
    'homepage' => '主页地址',
];
```
```php
$provider = Service::instance('module');
$provider = (new Service($config))->getProvider('module');
// 安装
$provider->install();
// 备份
$provider->backup();
// 升级
$provider->upgrade();
// 卸载
$provider->uninstall();

$server_api = [
	'base' => 'http://localhost:8081',
	'extra' => 'http://localhost:8082',
	// third 
];

$auth = [
	'base' => false,
	'extra' => [
		'username' => '',
		'password' => '',
	],
	'third' => 'callback'
],

$package = [
	'type' => 'module', // theme, plugin, hook, taglib
	'name' => 'test',
	'package_url' => 'http://localhost:8083/package/test.zip', // 下载地址
	'package_type' => 'zip',
	'requires' => [
		'module' => [
			'test1' => '>=0.01',
 		],
		'hook' => [
			'admin' => '>=0.01',
		]
	]
];

1. 安装
// 调用install方法
$provider->install();

2. 获取package 信息
$package = new Package($type, $name);
$info = $package->info(); // POST: 'http://localhost:8082/info.php', ['name'=>'', 'type'=>'', 'auth'=>[]]

3. 获取到包信息后, 检查依赖
$requires = $package->requires($info); // 要防止重复获取，并且读取本地已经安装的包
A->B   B->A
$requires = [
	'A' => [
		'B' => [
			'version' => '',
			'name' => '',
			'type' => '',
			'package_url' => '',
		],
	]
];
$requireMap = [
	'A' => 'B',
	'B' => 'A',
];

A->B B->C, B->D,  C->D
$requires = [
	'A' => [
		'B' => [
			'version' => '',
			'name' => '',
			'type' => '',
			'package_url' => '',
		],
	],
	'B' => [
		'C' => [
			'version' => '',
			'name' => '',
			'type' => '',
			'package_url' => '',
		],
		'D' => [
			'version' => '',
			'name' => '',
			'type' => '',
			'package_url' => '',
		],
	],
	'C' => [
		'D' => [
			'version' => '',
			'name' => '',
			'type' => '',
			'package_url' => '',
		],
	],
];
$requireMap = [
	'A' => ['B'],
	'B' => ['C', 'D'],
	'C' => 'D',
];

4. 下载未安装的
$package->download($path); // zip
$packageType = ZipPackage(),
$packageType->download($path);

5. 开启沙箱
$package->sandbox('open');
6. 解压
$package->unpack($path);
7. 验证
$package->verify($force);
// 验证通过
$package->commit();
// 验证不通过
$package->rollback(); // rmdir(); throw new \Exception($package->getError());
8. 关闭沙箱
$package->sandbox('close');
9. 执行插件安装
$provider->service->install();
10. 刷新
$package->refresh();

卸载
$provider->uninstall($type, $name, $backup);
// 先备份
$provider->backup($type, $name);
$package->backup($type, $name); // 备份代码
$provider->service->backup();	// 业务备份
$provider->service->uninstall();
// 不备份
$provider->service->uninstall()
$package->refresh();


备份
$provider->backup($type, $name);
$package->backup($type, $name);
$provider->service->backup();

// 升级
$provider->upgrade($type, $name);
$package->backupWithRequires($type, $name);
$package->sandbox('open');
... 安装流程

... 安装流程结束
$package->sandbox('close');
$provider->service->upgrade();
$package->refresh();
```