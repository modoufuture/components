<?php

namespace modoufuture\components\addons;

class Service
{
    /**
     * @var \modoufuture\components\addons\Config
     */
    protected $config;
    /**
     * @var string $type
     */
    protected $type;
    protected $event;

    public function __construct($config = [])
    {
        $this->config = new Config($config);
    }

    public function getProvider($provider)
    {
        $class = new Provider($this, strtolower($provider));

        return $class;
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }

        throw new \Exception(get_class($this).'::'.$name.'property not exists');
    }

    public function trigger($event, $args)
    {
        $events = $this->config->events;
        if (empty($events) || empty($events[$event])) {
            return;
        }
        foreach($events[$event] as $callback) {
            if (is_callable($callback)) {
                call_user_func_array($callback, $args);
            }
        }
    }
}