<?php

use modoufuture\components\addons\Service;

class Test
{
    public function install()
    {
        try {
            $provider = (new Service())->getProvider('module');
            $provider->install('test', []);
        } catch (\Exception $e) {

        }
    }
}