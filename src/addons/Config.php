<?php

namespace modoufuture\components\addons;

class Config
{
    protected $config = [
        'sandbox_path' => '',
        'tmp_path' => '',
        'save_path' => '',
        'repositories' => [
        ],
        'auth' => [
            'base' => false,
        ],
        'events' => [
            'install_init' => [],
            'install_unpack' => [],
            'service_install' => [],
        ]
    ];

    public function __construct($config)
    {
        $this->config = array_merge($this->config, $config);
    }

    public function __get($name)
    {
        $getter = 'get'.$name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name)) {
            return $this->$name;
        } else {
            return isset($this->config[$name]) ? $this->config[$name] : null;
        }
    }
}