<?php

namespace modoufuture\components\addons;

use GuzzleHttp\Client;
use Seld\JsonLint\JsonParser;

class Package
{
    /**
     * @var string $type
     */
    protected $type;
    /**
     * @var string $name
     */
    protected $name;
    /**
     * @var array $info
     */
    protected $info = [];
    /**
     * @var array $options
     */
    protected $options = [];
    /**
     * @var array $install_package
     */
    protected static $install_package = [];
    /**
     * @var bool
     */
    protected $sandbox = false;

    /**
     * @var null|array package lock info
     */
    protected static $package_lock = null;
    protected static $service;
    protected static $path = '';

    public function __construct($type, $name, $options = [])
    {
        $this->type = $type;
        $this->name = $name;
        $this->options = $options;
    }

    /**
     * get package info
     * @return array
     * @throws \Exception
     */
    public function info()
    {
        $key = md5($this->type.'_'.$this->name);
        if (!empty(static::$package_lock[$key])) {
            return $this->info = static::$package_lock[$key];
        }
        $header = [
            'User-Agent' => 'modoufuture/1.0 (modoucore)',
            'Accept'     => 'application/json',
        ];

        $options['json'] = array_merge(!empty($options['json'])?$options['json']:[],['name'=>$this->name, 'type'=>$this->type]);
        $options['header'] = array_merge(!empty($options['header'])?$options['header']:[], $header);
        $this->request('/info', $options, function($data){
            if (empty($data['data'])) {
                throw new \Exception(sprintf('package %s(%s) not found', $this->name, $this->type));
            }
            $this->info = $data['data'];
        }, true);
        if (empty($this->info)) {
            throw new \Exception(sprintf('package %s(%s) not found', $this->name, $this->type));
        }
        static::$package_lock[$key] = $this->info;
        static::$install_package[$key] = $this->info;

        return $this->info;
    }

    /**
     * @param array $info package info
     *                    eg. ['name'=>'', 'type'=>'', 'requires'=>['module'=>['test'=>'>=0.01']]]
     *
     * @return array
     */
    public function requires($info)
    {
        if (empty($info['requires'])) {
            return [];
        }

        foreach($info['requires'] as $type => $requires) {
            foreach($requires as $name => $version) {
                $key = md5($type.'_'.$name);
                if (isset(static::$package_lock[$key])) {
                    list($operator, $version_num) = $this->parseVersion($version);
                    if (version_compare(static::$package_lock[$key]['version'], $version_num, $operator) < 0) {
                        throw new \Exception(sprintf(
                            'your $s version is outdated. current version %s need %s',
                            $name.'('.$type.')',
                            static::$package_lock[$key]['version'],
                            $version
                        ));
                    }
                } else { // 获取 包信息
                    // get package info
                    $package = new static($type, $name, array_merge($this->options, ['json'=>['version'=>$version]]));
                    $info = $package->info();
                    $package->requires($info);
                }
            }
        }

        return $this->requires;
    }

    /**
     * @param $path
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function download()
    {
        $header = [
            'User-Agent' => 'modoufuture/1.0 (modoucore)',
        ];

        $options['header'] = array_merge(!empty($this->options['header'])?$this->options['header']:[], $header);
        $json = !empty($this->options['json'])?$this->options['json']:[];
        foreach(static::$install_package as $key=>$package){
            $options['json'] = array_merge($json,['name'=>$package['name'], 'type'=>$package['type']]);
            $file = static::$path . '/' . $package['type'] . '/' . $package['name'] . '.zip';
            $this->request($package['source']['url'], $options, function($data) use($file){
                if (empty($data)) {
                    throw new \Exception(sprintf('package %s(%s) download failed', $this->name, $this->type));
                }
                $dir = dirname($file);
                if (!is_dir($dir)) {
                    mkdir($dir, 0755, true);
                }
                file_put_contents($file, $data);
            }, false, 'GET');
        }
    }

    /**
     * @throws \Exception
     */
    public function unpack()
    {
        if (!class_exists('ZipArchive')) {
            throw new \Exception('Please install Zip extension');
        }
        foreach(static::$install_package as $key =>$package) {
            $path = static::$path . '/' . $package['type'].'/'.$package['name'];
            $file = $path . '.zip';
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            $zip = new \ZipArchive();
            if (true !== ($code = $zip->open($file))) {
                throw new \Exception($zip->getStatusString().', code:'.$code);
            }
            if (!$zip->extractTo($path)) {
                $zip->close();
                throw new \Exception('Unable to extract the file');
            }
            $zip->close();
            unlink($file);
        }
    }

    public function verify($force = false)
    {
        // 1. 验证所有的包有没有在安装目录下
        $save_path = static::$service->config->save_path;
        $error = [];
        foreach(static::$install_package as $key=>$package) {
            $path = $save_path . '/' . $package['type'] . '/' . $package['name'];
            if (is_dir($path) && !$force) {
                $error[] = sprintf('package %s(%s) already exists.', $package['name'], $package['type']);
            } elseif(!$this->verifyFile($path, $package)) {
                $error[] = sprintf('Incomplete package %s(%s).', $package['name'], $package['type']);
            }
        }

        return $error;
    }

    protected function verifyFile($save_path, $package)
    {
        $core_file = [
            ucfirst($package['name']).'.php',
            'info.php',
            'config.php'
        ];
        foreach($core_file as $k=>$v) {
            if (is_dir($save_path) && !is_file($save_path.'/'.$v)) {
                return false;
            }
            $file = static::$path.'/'.$package['type'].'/'.$package['name'].'/'.$v;
            if (!is_file($file)) {
                return false;
            }
        }

        return true;
    }

    public function lock()
    {
        file_put_contents(__DIR__.'/package.lock', json_encode(static::$package_lock, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    }

    /**
     * @param $version
     *
     * @return array
     */
    protected function parseVersion($version)
    {
        $operatories = ['<','lt','<=', 'le', '>', 'gt', '>=', 'ge', '==', '=', 'eq'];
        preg_match('/('.implode('|', $operatories).')\d/',$version, $matches);
        $operator = empty($matches[1]) ? '=' : $matches[1];
        $version = str_replace($operatories, '', $version);

        return [$operator, trim($version)];
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public static function getPackageLock()
    {
        if (is_null(static::$package_lock)) {
            $lock_file = __DIR__ . '/package.lock';
            $package_lock = [];
            if (is_file($lock_file)) {
                if (!is_readable($lock_file) || !is_writable($lock_file)) {
                    throw new \Exception('package lock file permission denied. file:'.$lock_file);
                }
                $parser = new JsonParser();
                $package_lock = file_get_contents($lock_file);
                if ($e = $parser->lint($package_lock)) {
                    throw new \Exception(sprintf("package lock file format error. \nfile:%s,\n details:%s", $lock_file, $e->getMessage()));
                }
                $package_lock = $parser->parse($package_lock, JsonParser::PARSE_TO_ASSOC);
            }
            static::$package_lock = $package_lock;
        }

        return static::$package_lock;
    }

    /**
     * @param       $uri
     * @param array $options
     * @param null  $callback
     * @param bool  $json
     *
     * @return mixed|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function request($uri, $options = [], $callback = null, $json = true, $method='POST')
    {
        $repositories = static::$service->config->repositories;
        foreach($repositories as $name => $repository) {
            try {
                $client = new Client();
                $this->requestAuthOption($name, $options);
                $uri = preg_match('/^http(s?):/i', $uri) ? $uri : $repository.$uri;
                $result = $client->request($method, $uri, $options);
                $content = $result->getBody()->getContents();
                if (empty($content)) {
                    throw new \Exception(sprintf('request failed!. server:%s, msg:%s', $name, $result->getReasonPhrase()));
                }
                if ($json) {
                    $content = json_decode($content, true);
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        throw new \Exception(sprintf('response format error!. server:%s, msg:%s', $name, json_last_error_msg()));
                    }
                }
                if ($callback instanceof \Closure) {
                    $callback($content);
                }

                return $content;
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                continue;
            }
        }
    }

    /**
     * @param $name
     * @param $options
     *
     * @return mixed
     */
    protected function requestAuthOption($name, &$options)
    {
        $auth = static::$service->config->auth;
        if (empty($auth[$name])) {
            return $options;
        }
        if (is_array($auth[$name])) {
            $options['auth'] = $auth[$name]; // https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html#auth
        } elseif (is_string($auth[$name]) && is_callable($auth[$name])) {
            $options = call_user_func_array($auth[$name], $options);
        }

        return $options;
    }

    /**
     * @param \modoufuture\components\addons\Service $service
     */
    public static function setService(Service $service)
    {
        static::$service = $service;
    }

    /**
     * @param string $type
     * @param string $name
     *
     * @return bool
     */
    public function alreadyInstalled($type='', $name='')
    {
        empty($type) && $type = $this->type;
        empty($name) && $name = $this->name;
        $key = md5($type.'_'.$name);

        return isset(static::$package_lock[$key]) && !isset(static::$install_package[$key]);
    }

    /**
     * @param bool $sandbox
     */
    public function sandbox($sandbox)
    {
        $this->sandbox = $sandbox;
    }

    public static function setPath($path)
    {
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        static::$path = $path;
    }

    public static function getInstallPackage()
    {
        return static::$install_package;
    }
}