<?php

namespace modoufuture\components\addons;

use modoufuture\utils\Dir;

class Provider
{
    /**
     * @var \modoufuture\components\addons\Service $service
     */
    protected $service;
    /**
     * @var \modoufuture\components\addons\Addons $addon
     */
    protected $addon;
    /**
     * @var string $type
     */
    protected $type;

    public function __construct(Service $service, $type)
    {
        $this->service = $service;
        $this->type = strtolower($type);
    }

    public function install($name, $force = false, $options=[])
    {
        $this->service->trigger('install_init', ['name'=>$name, 'options'=>$options]);
        Package::setService($this->service);
        $path = $this->service->config->tmp_path;
        $uqname = md5($this->type.'_'.$name);
        $path .= $uqname;
        Package::setPath($path);
        try {
            Package::getPackageLock();
            $package = new Package($this->type, $name, $options);
            // 获取package 信息
            $info = $package->info();
            if (!$force && $package->alreadyInstalled()) {
                throw new \Exception(sprintf('package %s(%s) already installed', $name, $this->type));
            }
            $package->requires($info);
            // Download all new packages
            $package->download();
            // 解压
            $package->unpack();
            if ($error = $package->verify($force)) {
                throw new \Exception(implode("\n", $error));
            }
            $this->service->trigger('install_unpack', ['path'=>$path, 'package'=>$package]);
        } catch (\Exception $e) {
            Dir::rm($path, true);
            throw $e;
        }
        // 复制代码
        $this->copy($path, $this->service->config->save_path);
        // 执行业务处理
        $this->service->trigger('service_install', ['package'=>$package, 'service'=>$this->service]);
        $package->lock();
        Dir::rm($path, true);

        return true;
    }

    public function copy($source, $dest)
    {
        if (!is_dir($dest)) {
            mkdir($dest, 0755, true);
        }
        $iterators = Dir::scanRecursive($source);
        foreach($iterators as $file) {
            if ($file->isDir()) {
                $subpath = $dest.'/'.$iterators->getSubPathName();
                if (!is_dir($subpath)) {
                    mkdir($subpath, 0755, true);
                }
            } else {
                copy($file, $dest . '/' . $iterators->getSubPathName());
            }
        }
    }
}