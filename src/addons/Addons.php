<?php

namespace modoufuture\components\addons;

interface Addons
{
    /**
     * @return bool
     */
    public function install();

    /**
     * @return bool
     */
    public function uninstall();

    /**
     * @return bool
     */
    public function upgrade();

    /**
     * @return bool
     */
    public function verify();

    /**
     * @return bool
     */
    public function backup();
}