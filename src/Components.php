<?php

namespace modoufuture\components;
use DI\ContainerBuilder;
use DI\NotFoundException;

/**
 * Class Components
 *
 * @package modoufuture\components
 */
class Components
{
    protected static $inst;
    /**
     * @var array
     */
    protected $components = [];
    /**
     * @var \DI\Container
     */
    protected $container;

    /**
     * Components constructor.
     *
     * @param array $components
     */
    private function __construct($components = [])
    {
        $this->components = require __DIR__.'/config.php';
        $this->components = array_merge($this->components, $components);
        if (version_compare(PHP_VERSION, '7.0', '>=')) {
            $this->container = new \DI\Container();
        } else {
            $builder = new ContainerBuilder();
            $this->container = $builder->build();
        }
    }

    /**
     * @param array $components
     *
     * @return \modoufuture\components\Components
     */
    public static function instance($components = [])
    {
        if (!self::$inst) {
            self::$inst = new self($components);
        }

        return self::$inst;
    }

    /**
     * @param $name
     *
     * @return mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function get($name)
    {
        try {
            return $this->container->get($name);
        } catch (NotFoundException $e) {
            if (!empty($this->components[$name])) {
                return modou_di($name, $this->components[$name]);
            }

            throw $e;
        }
    }

    /**
     * @param       $name
     * @param       $value
     * @param array $config
     *
     * @return mixed
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function set($name, $value, $config = [])
    {
        if (!is_string($value)) {
            $value = $this->container->call($value, $this->getComponentConfig($name, $config));
        } else {
            $value = $this->container->make($value, $this->getComponentConfig($name, $config));
        }

        $this->container->set($name, $value);

        return $this->get($name);
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function has($name)
    {
        return $this->container->has($name);
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function getComponentConfig($name, $config = [])
    {
        if (isset($this->components[$name])) {
            $old_config = $this->components[$name];
            $config = array_merge((array)$old_config, (array)$config);
        }

        return $config;
    }
}