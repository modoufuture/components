<?php
if (!function_exists('modou_di')) {
    /**
     * @param null|string $name
     * @param null|array $config
     * @example
     *         modou_di()   // this return Components::instance()
     *         modou_di('log'); // 如果，已经set过了， 则会直接返回实例; 如果没有，则会返回 stdClass()
     *         modou_di('?log'); // 检测是否存在，返回  true / false
     *         modou_di('log', $config);  // 设置新的 实例 $config中需要包含 '_class'参数， 剩下的参数是 class需要的参数值
     *
     * @return bool|mixed|\modoufuture\components\Components|object
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    function modou_di($name=null, $config = null)
    {
        $instance = \modoufuture\components\Components::instance();
        if (!$name) {
            return $instance;
        }
        if (stripos($name, '?') === 0) {
            return $instance->has(substr($name, 1));
        }
        if (is_null($config)) {
            return $instance->get($name);
        }
        $config = $instance->getComponentConfig($name, $config);
        $class = !empty($config['_class']) ? $config['_class'] : '';
        if (!empty($class)) {
            unset($config['_class']);
            return $instance->set($name, $class, $config);
        }

        return $instance->set($name, \stdClass::class, $config);
    }
}

if (!function_exists('modou_di_init')) {
    /**
     * @param array $components
     *
     * @return \modoufuture\components\Components
     */
    function modou_di_init($components = [])
    {
        return \modoufuture\components\Components::instance($components);
    }
}